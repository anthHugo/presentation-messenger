# Composant Messenger

## Installer symfony
```
composer create-project symfony/skeleton demo
cd demo
sudo php -S localhost:666 -t public -d xdebug.remote_enable=1 
```

## Installer le composant
```
 composer require symfony/messenger
```

Le composant inclus
  - symfony/messenger
  - symfony/redis-messenger
  - symfony/doctrine-messenger
  - symfony/amqp-messenger

#### Lancement de la démo
```
composer require doctrine/annotations
```

Message
```
cat <<EOT >> src/Message.php
<?php

declare(strict_types=1);

namespace App;

final class Message
{
    public function __construct(
        private string \$to,
        private string \$content,
    ) {
    }

    public function getTo(): string
    {
        return \$this->to;
    }

    public function getContent(): string
    {
        return \$this->content;
    }
}
EOT
```

Publisher
```
cat <<EOT >> src/Controller/PublisherController.php
<?php
            
namespace App\Controller;

use App\Message;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class PublisherController
{
    public function __construct(private MessageBusInterface \$bus)
    {
    }

    /**
     * @Route("/publisher", name="publisher")
     */
    public function __invoke(): JsonResponse
    {
        \$message = new Message(
            'hugo.casabella@opera-energie.com',
            'Présentation du publisher'
        );

        \$this->bus->dispatch(\$message);

        return new JsonResponse(["Publisher"]);
    }
}
EOT
```

Handler
```
cat <<EOT >> src/EmailMessageHandler.php
<?php
            
namespace App;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EmailMessageHandler implements MessageHandlerInterface
{
    public function __invoke(Message \$message)
    {
        echo 'envoi email';
    }
}
EOT
```

```
cat <<EOT >> src/SlackMessageHandler.php
<?php
            
namespace App;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SlackMessageHandler implements MessageHandlerInterface
{
    public function __invoke(Message \$message)
    {
        echo 'envoi notification slack';
    }
}
EOT
```

Editer config/packages/messenger.yaml pour ajouter
`'App\Message': sync`

[http://localhost:666/publisher](http://localhost:666/publisher)

# Presentation avec RabbitMQ
- activer async `'App\Message': async`
- lancer `curl http://localhost:666/publisher`
- se rendre sur [http://localhost:15672/](http://localhost:15672/)
- lancer le consumer : `php bin/console messenger:consume async -vv`