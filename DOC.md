# Symfony Messenger

Le composant Messenger aide les applications à envoyer et à recevoir des messages vers / depuis d'autres applications ou via des files d'attente de messages.

## Le concept
![logo](./schema.png)


#### Sender (Expéditeur)
Responsable de la sérialisation et de l'envoi de messages de "quelque chose". Ce quelque chose peut être un courtier de messages ou une API tierce par exemple.

#### Receiver (Récepteur)
Responsable de la récupération, de la désérialisation et de la transmission des messages au Handler. Un extracteur de file de messages ou un endpoint d'API par exemple.

#### Handler (Gestionnaire)
Responsable du traitement des messages en utilisant la logique métier applicable aux messages. Les handler sont appelés par le HandleMessageMiddleware.

#### Enveloppe
Concept spécifique à Messenger, il donne une flexibilité totale à l'intérieur du bus de messages, en y enveloppant les messages, permettant d'ajouter des informations utiles à l'intérieur grâce à des timbres d'enveloppe .

#### Timbres d'enveloppe
Information que vous devez joindre à votre message: contexte du sérialiseur à utiliser pour le transport, marqueurs identifiant un message reçu ou toute sorte de métadonnées que votre middleware ou couche de transport peut utiliser.

## Bus
Le bus est utilisé pour dispatch les messages. Le comportement du bus est géré par une stack de middleware fournis par le composant. (SendMessageMiddleware, HandleMessageMiddleware) 

#### Exemple
```php
use App\Message\MyMessage;
use App\MessageHandler\MyMessageHandler;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

$handler = new MyMessageHandler();

$bus = new MessageBus([
    new HandleMessageMiddleware(new HandlersLocator([
        MyMessage::class => [$handler],
    ])),
]);

$bus->dispatch(new MyMessage(/* ... */));
```

## Handlers
Une fois envoyés dans le bus, les messages seront traités par un «gestionnaire de messages». Un gestionnaire de messages est une closure. 

```php
namespace App\MessageHandler;

use App\Message\MyMessage;

class MyMessageHandler
{
    public function __invoke(MyMessage $message)
    {
        // Message processing...
    }
}
```

## Transports
Pour envoyer et recevoir des messages, vous devrez configurer un transport. Un transporteur sera chargé de communiquer avec votre courtier de messages ou des tiers.

```yaml
transports:
            yours: 'my-transport://...'
```